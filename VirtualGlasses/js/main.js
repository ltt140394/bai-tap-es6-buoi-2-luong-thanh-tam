let dataGlasses = [
    { id: "G1", src: "./img/g1.jpg", virtualImg: "./img/v1.png", brand: "Armani Exchange", name: "Bamboo wood", color: "Brown", price: 150, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? " },
    { id: "G2", src: "./img/g2.jpg", virtualImg: "./img/v2.png", brand: "Arnette", name: "American flag", color: "American flag", price: 150, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G3", src: "./img/g3.jpg", virtualImg: "./img/v3.png", brand: "Burberry", name: "Belt of Hippolyte", color: "Blue", price: 100, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G4", src: "./img/g4.jpg", virtualImg: "./img/v4.png", brand: "Coarch", name: "Cretan Bull", color: "Red", price: 100, description: "In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G5", src: "./img/g5.jpg", virtualImg: "./img/v5.png", brand: "D&G", name: "JOYRIDE", color: "Gold", price: 180, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?" },
    { id: "G6", src: "./img/g6.jpg", virtualImg: "./img/v6.png", brand: "Polo", name: "NATTY ICE", color: "Blue, White", price: 120, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G7", src: "./img/g7.jpg", virtualImg: "./img/v7.png", brand: "Ralph", name: "TORTOISE", color: "Black, Yellow", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam." },
    { id: "G8", src: "./img/g8.jpg", virtualImg: "./img/v8.png", brand: "Polo", name: "NATTY ICE", color: "Red, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim." },
    { id: "G9", src: "./img/g9.jpg", virtualImg: "./img/v9.png", brand: "Coarch", name: "MIDNIGHT VIXEN REMIX", color: "Blue, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet." }
];

const renderGlasses = () => {
    let contentHTML = "";

    dataGlasses.forEach((item) => {
        let contentGlassesList = /*html */`
        <div class="vglasses__items col-4" onclick="changeGlasses('${item.id}')">
            <img src=${item.src} alt="Glasses_PicTure">
        </div>
        `;
        contentHTML += contentGlassesList;

    });

    document.getElementById("vglassesList").innerHTML = contentHTML;
}

renderGlasses();


const changeGlasses = (id) => {
    let imgEl = document.getElementById("glassesPicked");

    let index = dataGlasses.findIndex((item) => {
        return item.id == id;
    });

    imgEl.src = dataGlasses[index].virtualImg;

    let contentHTML = /*html */ `
    <h5>${dataGlasses[index].name} - ${dataGlasses[index].brand} (${dataGlasses[index].color})</h5>
    <button class="btn btn-danger">$${dataGlasses[index].price}</button> <span class="text-success">Stocking</span>
    <p class="mt-4">${dataGlasses[index].description}</p>
    `;

    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("glassesInfo").innerHTML = contentHTML;
    document.getElementById("glassesPicked").style.display = "block";

    // lấy id của glasses để check xem đã chọn kính hay chưa để tránh hiện khung img chưa có src khi chưa chọn kính mà click vào button after
    document.getElementById("checkPickedOrNot").innerText = dataGlasses[index].id;
    document.getElementById("checkPickedOrNot").style.display = "none";
}

const removeGlasses = (boolean) => {
    let isPicked = document.getElementById("checkPickedOrNot").innerText;

    if (boolean && isPicked) {
        document.getElementById("glassesPicked").style.display = "block";
    } else {
        document.getElementById("glassesPicked").style.display = "none";
    }
}



